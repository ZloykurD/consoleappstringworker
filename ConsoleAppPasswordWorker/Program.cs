﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppPasswordWorker
{
    class Program
    {
        //Задача №2 
        /*
        Рассмотрим следующий код, который отвечает за кодирование строки. Предположим, что он используется для сохранения пользовательских паролей.

        public class Encoder
        {
            public string EncodeString(string str)
            {
                string res = "";
                foreach (char ch in str)
                {
                    char s = ch;
                    s++;
                    res += s;
                }
                return res;
            }
        }

    Данный код каждый символ переводит в последующий символ. Таким образом строка abc переводится в строку bcd.

    Один из клиентов хочет использовать более надежное кодирование паролей – а именно хэш-функцию SHA-1. Опишите изменения – что необходимо изменить в коде,
    чтобы для одного клиента использовалась новая функция SHA-1, а для остальных клиентов – осталась существующая функция. Объясните Ваше решение.
    В целях данной задачи – можно предположить, что уже есть метод, который по входной строке формирует ее хэш-значение. 

         */
        static void Main(string[] args)
        {

            // я могу предположить что при вводе пароля у пользователя есть выбор в качестве ЧекБокса который предлагает выбрать более защищенный вариант кодирования пароля
            //если пользователь указал Checked мы получаем значение true и передаем параметром isSecurity
            bool isSecurity = 2 == new Random().Next(3);

            Console.WriteLine(isSecurity ? Encoder.EncodeString("absd") : Encoder.EncodeSecurityString("absd"));
            Console.ReadKey();
        }
    }
    
    static class Encoder
    {


        public static string EncodeString(string password)
        {
            StringBuilder
                builder =
                    new StringBuilder(); //правильный вариант: класс представляет динамическую строку т.е. будет затрачено меньше места в куче
            string
                res = ""; //не правильный вариант: т.е. строка представляет собой объект и будет пересоздаваться с каждой итерацией и потребует место в куче для каждого изминения
            foreach (char ch in password)
            {
                char s = ch;
                s++;
                //res += s; X
                builder.Append(s);
            }

            //return res; X
            return builder.ToString();
        }


        public static string EncodeSecurityString(string password)
        {
            SHA1 sha1 = SHA1.Create();
            StringBuilder builder = new StringBuilder();
            byte[] inputBytes = Encoding.ASCII.GetBytes(password);
            byte[] hash = sha1.ComputeHash(inputBytes);
            for (int i = 0; i < hash.Length; i++)
            {
                builder.Append(hash[i].ToString("X2"));
            }

            return builder.ToString();
        }
    }
}
