﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppStringWorker
{
    class Program
    {        //Задача №1
        /*
          1.	Напишите метод, который будет «переворачивать» строку.
          Т.е. из строки abcd получать строку dcba. Оцените сложность данного алгоритма.
          Задачу необходимо реализовать без использования стандартных методов:
          Array.Reverse, string.Reverse и аналогичных.
         */
        private static bool isEnd = false;
        private static string input = string.Empty;

        static void Main(string[] args)
        {
            GetColorState(ConsoleColor.Cyan);
            Console.WriteLine("Welcome! Please write some text for Upend ");
            GetColorState(ConsoleColor.Yellow);
            Console.WriteLine("Rools: text need write >= 3 Length");
            GetStart();
            GetColorState(ConsoleColor.Cyan);
            Console.WriteLine("Program is end.");
            Console.ReadKey();
        }

        static void GetStart()
        {
            input = string.Empty;

            while (!isEnd)
            {
                GetColorState(ConsoleColor.White);
                input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    GetColorState(ConsoleColor.Red);
                    Console.WriteLine("error: need write some text!");
                }
                else
                {
                    if (input.Length < 3)
                    {
                        GetColorState(ConsoleColor.Red);
                        Console.WriteLine("error: need write >= 3 Length");
                    }
                    else
                    {
                        GetUpendString(input);
                        isEnd = true;
                    }
                }
            }
        }

        static void GetUpendString(string input)
        {
            string
                result = string
                    .Empty; //не правильный вариант: т.е. строка представляет собой объект и будет пересоздаваться с каждой итерацией и потребует место в куче для каждого изминения
            StringBuilder
                builder =
                    new StringBuilder(); //правильный вариант: класс представляет динамическую строку т.е. будет затрачено меньше места в куче

            for (int i = input.Length; i > 0; i--)
            {
                //Console.WriteLine(result +=input[i-1]); X
                builder.Append(input[i - 1]);
            }

            GetColorState(ConsoleColor.Green);
            Console.WriteLine(builder.ToString());
            //Console.WriteLine(result); X
        }

        static void GetColorState(ConsoleColor color)
        {
            Console.ResetColor();
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = color;
        }
    }
}